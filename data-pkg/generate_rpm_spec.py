#!/usr/bin/env python
import argparse
import os
from LbRPMTools.LHCbRPMSpecBuilder import LHCbDatapkgRpmSpec
from LbRPMTools.LHCbRPMSpecBuilder import RpmDirConfig


def generate_spec(project, package, version, src_zip_fn, build_area):
    spec = LHCbDatapkgRpmSpec(project, package, version, src_zip_fn, build_area)
    rpmbuildname = package.replace("/", "_")
    rpmconf = RpmDirConfig(build_area, rpmbuildname)
    specfilename = os.path.join(rpmconf.topdir, rpmbuildname + ".spec")
    with open(specfilename, "wt") as outputfile:
        outputfile.write(spec.getSpec())


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("project")
    parser.add_argument("package")
    parser.add_argument("version")
    parser.add_argument("src_zip_fn")
    parser.add_argument("build_area")
    args = parser.parse_args()

    generate_spec(
        args.project, args.package, args.version, args.src_zip_fn, args.build_area
    )


if __name__ == "__main__":
    main()
