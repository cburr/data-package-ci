#!/usr/bin/env bash
# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

yum install -y python-pip zip unzip rpm-build
python -m pip install --upgrade pip
python -m pip install --upgrade setuptools
python -m pip install LbNightlyTools requests requests_toolbelt
